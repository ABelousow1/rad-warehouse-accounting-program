﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using System.Data.Entity;

namespace RadBelousov.Entities
{
    public class Context: DbContext
    {
        public DbSet<Vendor> Vendors { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<ClientPurchase> ClientPurchases { get; set; }
        public DbSet<WareHouseState> WareHouseStates { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Purchase> Purchases { get; set; }
        public DbSet<PurchaseInvoice> PurchaseInvoices { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<WareHouseState>().HasKey("DateTime", "Amount");
            modelBuilder.Entity<WareHouseState>().HasKey(t => new { t.DateTime, t.Amount});
            modelBuilder.Entity<ClientPurchase>().HasKey(t => t.OrderId);
        }
        public Context(): base(nameOrConnectionString: "PGConnection") {
            //Database.Delete();
            //Database.Create();
           //Database.EnsureDeleted();
           // Database.EnsureCreated();
        }
        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    //optionsBuilder.UseNpgsql(@"host=localhost;port=5432;database=EntityDbRad;username=postgres;password=1123");
            
        //}
    }
}
