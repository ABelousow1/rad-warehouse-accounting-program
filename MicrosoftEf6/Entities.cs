﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicrosoftEf6
{
    public class Category
    {
        private readonly ObservableListSource<Product> _products =
                new ObservableListSource<Product>();

        public int CategoryId { get; set; }
        public string Name { get; set; }
        public virtual ObservableListSource<Product> Products { get { return _products; } }
    }
    public class Product
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public decimal? Price { get; set; }
        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }
        public int Amount { get; set; }
        public Product() { }
        public Product(Product p)
        {
            ProductId = p.ProductId;
            Name = p.Name;
            Price = p.Price;
            CategoryId = p.CategoryId;
            Category = p.Category;
            Amount = p.Amount;
        }
        public Product GetInstance(Product p)
        {
            return new Product(p);
        }
    }
    public class InvoicePurchase
    {
        public int InvoicePurchaseId { get; set; }
        public int PurchaseInvoiceId { get; set; }
        public int Amount { get; set; }
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
    }
    public class PurchaseInvoice
    {
        private readonly ObservableListSource<InvoicePurchase> _purchases =
                new ObservableListSource<InvoicePurchase>();
        public int PurchaseInvoiceId { get; set; }
        public DateTime DateTime { get; set; }
        public virtual ObservableListSource<InvoicePurchase> Purchases { get { return _purchases; } }
    }
    public class WareHouse
    {
        [Column("ProductOnWareHouse_ProductId")]
        public int ProductId { get; set; }
        public virtual Product ProductOnWareHouse { get; set; }
        public DateTime ChangeTimeStamp { get; set; }
        public int Amount { get; set; }
        public WareHouse() { }
        public WareHouse(WareHouse wh)
        {
            this.ProductId = wh.ProductId;
            this.ProductOnWareHouse = wh.ProductOnWareHouse;
            this.ChangeTimeStamp = wh.ChangeTimeStamp;
            this.Amount = wh.Amount;
        }
        public WareHouse getInstance(WareHouse wh)
        {
            return new WareHouse(wh);
        }
    }
    public class OrderPurchase
    {
        public int OrderPurchaseId { get; set; }

        [ForeignKey("Order")]
        public int OrderId { get; set; }
        public virtual Order Order { get; set; }
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
        public int Amount { get; set; }
        public decimal? totalPrice {
            get 
            { 
               if (Product != null) 
                    return Amount * Product.Price; 
                else return null; 
            } 
        }
    }
    public class Order
    {
        private readonly ObservableListSource<OrderPurchase> _orderPurchases = new
            ObservableListSource<OrderPurchase>();
        public int OrderId { get; set; }
        public int ClientId { get; set; }
        public virtual Client Client { get; set; }
        public virtual ObservableListSource<OrderPurchase> OrderPurchases { get { return _orderPurchases; } }
        public DateTime DateTime { get; set; }
        public string Status { get; set; } = "Active";
        public decimal? TotalPrice 
        { 
            get 
            { 
                return _orderPurchases.Sum(x => x.totalPrice); 
            } 
        }
    }
    public class Client
    {
        public int ClientId { get; set; }   
        public string ClientName { get; set; }

        private readonly ObservableListSource<Order> _orders =
                new ObservableListSource<Order>();
        public virtual ObservableListSource<Order> Orders { get { return _orders; } }

    }
}
