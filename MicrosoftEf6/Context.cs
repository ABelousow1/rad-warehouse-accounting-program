﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace MicrosoftEf6
{
    public class Context: DbContext
    {
        public Context() : base(nameOrConnectionString: "PGconn") {
           // Database.Delete();
           //Database.Create();
          // Database.Initialize(false);
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("public");
            modelBuilder.Entity<WareHouse>().HasKey(entity => new { entity.ChangeTimeStamp, entity.ProductId});
           // modelBuilder.Entity<PurchaseInvoice>().HasKey(entity => entity.InvoiceId);
           // modelBuilder.Entity<OrderPurchase>().HasKey(entity => entity.PurchaseId);

            base.OnModelCreating(modelBuilder);
            
        }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<PurchaseInvoice> Invoices { get; set; }
        public DbSet<InvoicePurchase> InvoicePurchases { get; set; }
        public DbSet<OrderPurchase> OrderPurchases { get; set; }
        public DbSet<WareHouse> WareHouse { get; set; }
    }
}
