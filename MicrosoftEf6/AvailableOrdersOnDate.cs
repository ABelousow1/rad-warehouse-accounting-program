﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Interop.Word;

namespace MicrosoftEf6
{
    public partial class AvailableOrdersOnDate : Form
    {
        WareHouse[] wareHouses;
        DateTime date;
        Context _ctx;
        List<Order> availableOrders;
        public AvailableOrdersOnDate(DateTime dt, Context ctx): this()
        {
            _ctx = ctx;
            date = dt;
            DateLable.Text = date.ToString();
        }
        public AvailableOrdersOnDate()
        {
            InitializeComponent();
            this.Refresh();
        }

        private void AvailableOrdersOnDate_Load(object sender, EventArgs e)
        {
            _ctx.WareHouse.Load();
            _ctx.Products.Load();
            _ctx.Orders.Load();
            List<WareHouse> wh = new List<WareHouse>();
            Product[] products = _ctx.Products.ToArray();
            foreach (Product product in products)
            {
                var obj = _ctx.WareHouse.Where(p => p.ChangeTimeStamp <= date && p.ProductId == product.ProductId).OrderByDescending(p => p.ChangeTimeStamp).FirstOrDefault();
                if (obj != null)
                    wh.Add(new WareHouse(obj));
            }
            wareHouses = new WareHouse[wh.Count];
            wh.CopyTo(wareHouses);
            Order[] Orders = _ctx.Orders.Where(x => x.DateTime <= date).OrderByDescending(x => x.DateTime).ToArray();
            
            availableOrders = new List<Order>(); 

            foreach (Order order in Orders)
            {
                var orderPurchases = _ctx.OrderPurchases
                    .Where(ord => ord.OrderId == order.OrderId).ToList();
                if (orderPurchases
                    .All(x => x.Amount <= wareHouses.Where(p => p.ProductId == x.ProductId).First().Amount))
                {
                    availableOrders.Add(order);
                    foreach (var wHouse in wareHouses
                        .Where(p => orderPurchases.
                        Where(q => q.OrderId == order.OrderId)
                        .Select(y => y.ProductId).Contains(p.ProductId)))
                    {
                        wHouse.Amount -= orderPurchases.Where(p => p.ProductId == wHouse.ProductId).FirstOrDefault().Amount;
                    }
                }

            }
            _ctx.Clients.Load();
            clientBindingSource.DataSource = _ctx.Clients.ToList();
            orderBindingSource.DataSource = availableOrders;
            productBindingSource.DataSource = _ctx.Products.ToList();
        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (var Ord in availableOrders)
            {
                Ord.Status = "done";
            }
            foreach (var wh in wareHouses)
            {
                wh.ChangeTimeStamp = date;
            }
            _ctx.WareHouse.AddRange(wareHouses.Where(x => x.Amount!=0).ToList());
            var prlst = _ctx.Products.Local.ToBindingList();
            var pr2 = prlst.Where(p => wareHouses.Select(x => x.ProductId).Contains(p.ProductId));
            foreach (var wh in wareHouses)
            {
                pr2.Where(p => p.ProductId == wh.ProductId).FirstOrDefault().Amount -= wh.Amount;
            }
            _ctx.SaveChanges();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            object oMissing = System.Reflection.Missing.Value;
            object oEndOfDoc = "\\endofdoc"; /* \endofdoc is a predefined bookmark */
            //Start Word and create a new document.
            Microsoft.Office.Interop.Word._Application oWord;
            Microsoft.Office.Interop.Word._Document oDoc;
            oWord = new Microsoft.Office.Interop.Word.Application();
            oWord.Visible = true;
            oDoc = oWord.Documents.Add(ref oMissing, ref oMissing,
            ref oMissing, ref oMissing);

            //Insert a paragraph at the beginning of the document.
            Microsoft.Office.Interop.Word.Paragraph oPara1;
            oPara1 = oDoc.Content.Paragraphs.Add(ref oMissing);
            oPara1.Range.Text = "Список заказов, доступых для выполнения на дату";
            oPara1.Range.Font.Bold = 1;
            oPara1.Format.SpaceAfter = 12;    //24 pt spacing after paragraph.
            oPara1.Range.InsertParagraphAfter();

            //Insert a paragraph at the end of the document.
            Microsoft.Office.Interop.Word.Paragraph oPara2;
            object oRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            oPara2 = oDoc.Content.Paragraphs.Add(ref oRng);
            oPara2.Range.Text = date.ToString();
            oPara2.Format.SpaceAfter = 6;
            oPara2.Range.InsertParagraphAfter();

            //Insert another paragraph.
            Microsoft.Office.Interop.Word.Paragraph oPara3;
            oRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            oPara3 = oDoc.Content.Paragraphs.Add(ref oRng);
            oPara3.Range.Text = "Следующие заказы могут быть доставлены на текущую дату";
            oPara3.Range.Font.Bold = 0;
            oPara3.Format.SpaceAfter = 24;
            oPara3.Range.InsertParagraphAfter();

            //Insert a 3 x 5 table, fill it with data, and make the first row
            //bold and italic.
            Microsoft.Office.Interop.Word.Table oTable;
            Microsoft.Office.Interop.Word.Range wrdRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            oTable = oDoc.Tables.Add(wrdRng, dataGridView1.Rows.Count+1, dataGridView1.Columns.Count, ref oMissing, ref oMissing);
            oTable.Range.ParagraphFormat.SpaceAfter = 6;
            int r, c;
            string strText;
            oTable.Cell(0, 0).Range.Text = "Id клиента";
            oTable.Cell(0, 1).Range.Text = "Id заказа";
            oTable.Cell(0, 2).Range.Text = "Дата заказа";
            oTable.Cell(0, 3).Range.Text = "Сумма";
            //oTable.Cell(0, 3).Range.Text = "цена";

            for (r = 1; r < dataGridView1.Rows.Count+1; r++)
                for (c = 0; c < dataGridView1.Rows[r-1].Cells.Count; c++)
                { 
                    oTable.Cell(r, c).Range.Text = dataGridView1.Rows[r-1].Cells[c].Value.ToString();
                }
            oTable.Rows[1].Range.Font.Bold = 1;
            oTable.Rows[1].Range.Font.Italic = 1;
            
            //Add some text after the table.
            Microsoft.Office.Interop.Word.Paragraph oPara4;
            oRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            oPara4 = oDoc.Content.Paragraphs.Add(ref oRng);
            oPara4.Range.InsertParagraphBefore();
            //oPara4.Range.Text = "And here's another table:";
            //oPara4.Format.SpaceAfter = 24;
            //oPara4.Range.InsertParagraphAfter();

            ////Insert a 5 x 2 table, fill it with data, and change the column widths.
            //wrdRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            //oTable = oDoc.Tables.Add(wrdRng, 5, 2, ref oMissing, ref oMissing);
            //oTable.Range.ParagraphFormat.SpaceAfter = 6;
            //for (r = 1; r <= 5; r++)
            //    for (c = 1; c <= 2; c++)
            //    {
            //        strText = "r" + r + "c" + c;
            //        oTable.Cell(r, c).Range.Text = strText;
            //    }
            //oTable.Columns[1].Width = oWord.InchesToPoints(2); //Change width of columns 1 & 2
            //oTable.Columns[2].Width = oWord.InchesToPoints(3);

            ////Keep inserting text. When you get to 7 inches from top of the
            ////document, insert a hard page break.
            //object oPos;
            //double dPos = oWord.InchesToPoints(7);
            //oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range.InsertParagraphAfter();
            //do
            //{
            //    wrdRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            //    wrdRng.ParagraphFormat.SpaceAfter = 6;
            //    wrdRng.InsertAfter("A line of text");
            //    wrdRng.InsertParagraphAfter();
            //    oPos = wrdRng.get_Information
            //                           (Microsoft.Office.Interop.Word.WdInformation.wdVerticalPositionRelativeToPage);
            //}
            //while (dPos >= Convert.ToDouble(oPos));
            //object oCollapseEnd = Microsoft.Office.Interop.Word.WdCollapseDirection.wdCollapseEnd;
            //object oPageBreak = Microsoft.Office.Interop.Word.WdBreakType.wdPageBreak;
            //wrdRng.Collapse(ref oCollapseEnd);
            //wrdRng.InsertBreak(ref oPageBreak);
            //wrdRng.Collapse(ref oCollapseEnd);
            //wrdRng.InsertAfter("We're now on page 2. Here's my chart:");
            //wrdRng.InsertParagraphAfter();

            ////Insert a chart.
            //Microsoft.Office.Interop.Word.InlineShape oShape;
            //object oClassType = "MSGraph.Chart.8";
            //wrdRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            //oShape = wrdRng.InlineShapes.AddOLEObject(ref oClassType, ref oMissing,
            //ref oMissing, ref oMissing, ref oMissing,
            //ref oMissing, ref oMissing, ref oMissing);

            ////Demonstrate use of late bound oChart and oChartApp objects to
            ////manipulate the chart object with MSGraph.
            //object oChart;
            //object oChartApp;
            //oChart = oShape.OLEFormat.Object;
            //oChartApp = oChart.GetType().InvokeMember("Application",
            //BindingFlags.GetProperty, null, oChart, null);

            ////Change the chart type to Line.
            //object[] Parameters = new Object[1];
            //Parameters[0] = 4; //xlLine = 4
            //oChart.GetType().InvokeMember("ChartType", BindingFlags.SetProperty,
            //null, oChart, Parameters);

            ////Update the chart image and quit MSGraph.
            //oChartApp.GetType().InvokeMember("Update",
            //BindingFlags.InvokeMethod, null, oChartApp, null);
            //oChartApp.GetType().InvokeMember("Quit",
            //BindingFlags.InvokeMethod, null, oChartApp, null);
            ////... If desired, you can proceed from here using the Microsoft Graph 
            ////Object model on the oChart and oChartApp objects to make additional
            ////changes to the chart.

            ////Set the width of the chart.
            //oShape.Width = oWord.InchesToPoints(6.25f);
            //oShape.Height = oWord.InchesToPoints(3.57f);

            ////Add text after the chart.
            //wrdRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
            //wrdRng.InsertParagraphAfter();
            //wrdRng.InsertAfter("THE END.");

            ////Close this form.
            //this.Close();
        }
    }
}
