﻿namespace MicrosoftEf6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class added_more_entities : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "public.Clients",
                c => new
                    {
                        ClientId = c.Int(nullable: false, identity: true),
                        ClientName = c.String(),
                    })
                .PrimaryKey(t => t.ClientId);
            
            CreateTable(
                "public.InvoicePurchases",
                c => new
                    {
                        InvoicePurchaseId = c.Int(nullable: false, identity: true),
                        Amount = c.Int(nullable: false),
                        Product_ProductId = c.Int(),
                        PurchaseInvoice_InvoiceId = c.Int(),
                    })
                .PrimaryKey(t => t.InvoicePurchaseId)
                .ForeignKey("public.Products", t => t.Product_ProductId)
                .ForeignKey("public.PurchaseInvoices", t => t.PurchaseInvoice_InvoiceId)
                .Index(t => t.Product_ProductId)
                .Index(t => t.PurchaseInvoice_InvoiceId);
            
            CreateTable(
                "public.PurchaseInvoices",
                c => new
                    {
                        InvoiceId = c.Int(nullable: false, identity: true),
                        DateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.InvoiceId);
            
            CreateTable(
                "public.OrderPurchases",
                c => new
                    {
                        PurchaseId = c.Int(nullable: false, identity: true),
                        Amount = c.Int(nullable: false),
                        totalPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Order_OrderId = c.Int(),
                        Product_ProductId = c.Int(),
                    })
                .PrimaryKey(t => t.PurchaseId)
                .ForeignKey("public.Orders", t => t.Order_OrderId)
                .ForeignKey("public.Products", t => t.Product_ProductId)
                .Index(t => t.Order_OrderId)
                .Index(t => t.Product_ProductId);
            
            CreateTable(
                "public.Orders",
                c => new
                    {
                        OrderId = c.Int(nullable: false, identity: true),
                        DateTime = c.DateTime(nullable: false),
                        OrderingClient_ClientId = c.Int(),
                    })
                .PrimaryKey(t => t.OrderId)
                .ForeignKey("public.Clients", t => t.OrderingClient_ClientId)
                .Index(t => t.OrderingClient_ClientId);
            
            CreateTable(
                "public.WareHouses",
                c => new
                    {
                        ChangeTimeStamp = c.DateTime(nullable: false),
                        Amount = c.Int(nullable: false),
                        ProductOnWareHouse_ProductId = c.Int(),
                    })
                .PrimaryKey(t => t.ChangeTimeStamp)
                .ForeignKey("public.Products", t => t.ProductOnWareHouse_ProductId)
                .Index(t => t.ProductOnWareHouse_ProductId);
            
            AddColumn("public.Products", "Price", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("public.Products", "Client_ClientId", c => c.Int());
            CreateIndex("public.Products", "Client_ClientId");
            AddForeignKey("public.Products", "Client_ClientId", "public.Clients", "ClientId");
        }
        
        public override void Down()
        {
            DropForeignKey("public.WareHouses", "ProductOnWareHouse_ProductId", "public.Products");
            DropForeignKey("public.OrderPurchases", "Product_ProductId", "public.Products");
            DropForeignKey("public.OrderPurchases", "Order_OrderId", "public.Orders");
            DropForeignKey("public.Orders", "OrderingClient_ClientId", "public.Clients");
            DropForeignKey("public.InvoicePurchases", "PurchaseInvoice_InvoiceId", "public.PurchaseInvoices");
            DropForeignKey("public.InvoicePurchases", "Product_ProductId", "public.Products");
            DropForeignKey("public.Products", "Client_ClientId", "public.Clients");
            DropIndex("public.WareHouses", new[] { "ProductOnWareHouse_ProductId" });
            DropIndex("public.Orders", new[] { "OrderingClient_ClientId" });
            DropIndex("public.OrderPurchases", new[] { "Product_ProductId" });
            DropIndex("public.OrderPurchases", new[] { "Order_OrderId" });
            DropIndex("public.InvoicePurchases", new[] { "PurchaseInvoice_InvoiceId" });
            DropIndex("public.InvoicePurchases", new[] { "Product_ProductId" });
            DropIndex("public.Products", new[] { "Client_ClientId" });
            DropColumn("public.Products", "Client_ClientId");
            DropColumn("public.Products", "Price");
            DropTable("public.WareHouses");
            DropTable("public.Orders");
            DropTable("public.OrderPurchases");
            DropTable("public.PurchaseInvoices");
            DropTable("public.InvoicePurchases");
            DropTable("public.Clients");
        }
    }
}
