﻿namespace MicrosoftEf6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedIdEntities1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("public.OrderPurchases", "OrderId", "public.Orders");
            DropIndex("public.OrderPurchases", new[] { "OrderId" });
            AlterColumn("public.OrderPurchases", "OrderId", c => c.Int());
            CreateIndex("public.OrderPurchases", "OrderId");
            AddForeignKey("public.OrderPurchases", "OrderId", "public.Orders", "OrderId");
        }
        
        public override void Down()
        {
            DropForeignKey("public.OrderPurchases", "OrderId", "public.Orders");
            DropIndex("public.OrderPurchases", new[] { "OrderId" });
            AlterColumn("public.OrderPurchases", "OrderId", c => c.Int(nullable: false));
            CreateIndex("public.OrderPurchases", "OrderId");
            AddForeignKey("public.OrderPurchases", "OrderId", "public.Orders", "OrderId", cascadeDelete: true);
        }
    }
}
