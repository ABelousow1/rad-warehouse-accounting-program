﻿namespace MicrosoftEf6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changedClientPurchasesDGV : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("public.OrderPurchases", "OrderId", "public.Orders");
            DropForeignKey("public.OrderPurchases", "ProductId", "public.Products");
            DropIndex("public.OrderPurchases", new[] { "OrderId" });
            DropIndex("public.OrderPurchases", new[] { "ProductId" });
            AlterColumn("public.OrderPurchases", "OrderId", c => c.Int(nullable: false));
            AlterColumn("public.OrderPurchases", "ProductId", c => c.Int(nullable: false));
            CreateIndex("public.OrderPurchases", "OrderId");
            CreateIndex("public.OrderPurchases", "ProductId");
            AddForeignKey("public.OrderPurchases", "OrderId", "public.Orders", "OrderId", cascadeDelete: true);
            AddForeignKey("public.OrderPurchases", "ProductId", "public.Products", "ProductId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("public.OrderPurchases", "ProductId", "public.Products");
            DropForeignKey("public.OrderPurchases", "OrderId", "public.Orders");
            DropIndex("public.OrderPurchases", new[] { "ProductId" });
            DropIndex("public.OrderPurchases", new[] { "OrderId" });
            AlterColumn("public.OrderPurchases", "ProductId", c => c.Int());
            AlterColumn("public.OrderPurchases", "OrderId", c => c.Int());
            CreateIndex("public.OrderPurchases", "ProductId");
            CreateIndex("public.OrderPurchases", "OrderId");
            AddForeignKey("public.OrderPurchases", "ProductId", "public.Products", "ProductId");
            AddForeignKey("public.OrderPurchases", "OrderId", "public.Orders", "OrderId");
        }
    }
}
