﻿namespace MicrosoftEf6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedVirtualProperties : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "public.OrderPurchases", name: "Product_ProductId", newName: "ProductId");
            RenameIndex(table: "public.OrderPurchases", name: "IX_Product_ProductId", newName: "IX_ProductId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "public.OrderPurchases", name: "IX_ProductId", newName: "IX_Product_ProductId");
            RenameColumn(table: "public.OrderPurchases", name: "ProductId", newName: "Product_ProductId");
        }
    }
}
