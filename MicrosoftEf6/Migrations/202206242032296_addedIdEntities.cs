﻿namespace MicrosoftEf6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedIdEntities : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("public.Orders", "Client_ClientId", "public.Clients");
            DropForeignKey("public.OrderPurchases", "Order_OrderId", "public.Orders");
            DropForeignKey("public.InvoicePurchases", "Product_ProductId", "public.Products");
            DropIndex("public.Orders", new[] { "Client_ClientId" });
            DropIndex("public.OrderPurchases", new[] { "Order_OrderId" });
            DropIndex("public.InvoicePurchases", new[] { "Product_ProductId" });
            RenameColumn(table: "public.Orders", name: "Client_ClientId", newName: "ClientId");
            RenameColumn(table: "public.OrderPurchases", name: "Order_OrderId", newName: "OrderId");
            RenameColumn(table: "public.InvoicePurchases", name: "Product_ProductId", newName: "ProductId");
            AlterColumn("public.Orders", "ClientId", c => c.Int(nullable: false));
            AlterColumn("public.OrderPurchases", "OrderId", c => c.Int(nullable: false));
            AlterColumn("public.InvoicePurchases", "ProductId", c => c.Int(nullable: false));
            CreateIndex("public.Orders", "ClientId");
            CreateIndex("public.OrderPurchases", "OrderId");
            CreateIndex("public.InvoicePurchases", "ProductId");
            AddForeignKey("public.Orders", "ClientId", "public.Clients", "ClientId", cascadeDelete: true);
            AddForeignKey("public.OrderPurchases", "OrderId", "public.Orders", "OrderId", cascadeDelete: true);
            AddForeignKey("public.InvoicePurchases", "ProductId", "public.Products", "ProductId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("public.InvoicePurchases", "ProductId", "public.Products");
            DropForeignKey("public.OrderPurchases", "OrderId", "public.Orders");
            DropForeignKey("public.Orders", "ClientId", "public.Clients");
            DropIndex("public.InvoicePurchases", new[] { "ProductId" });
            DropIndex("public.OrderPurchases", new[] { "OrderId" });
            DropIndex("public.Orders", new[] { "ClientId" });
            AlterColumn("public.InvoicePurchases", "ProductId", c => c.Int());
            AlterColumn("public.OrderPurchases", "OrderId", c => c.Int());
            AlterColumn("public.Orders", "ClientId", c => c.Int());
            RenameColumn(table: "public.InvoicePurchases", name: "ProductId", newName: "Product_ProductId");
            RenameColumn(table: "public.OrderPurchases", name: "OrderId", newName: "Order_OrderId");
            RenameColumn(table: "public.Orders", name: "ClientId", newName: "Client_ClientId");
            CreateIndex("public.InvoicePurchases", "Product_ProductId");
            CreateIndex("public.OrderPurchases", "Order_OrderId");
            CreateIndex("public.Orders", "Client_ClientId");
            AddForeignKey("public.InvoicePurchases", "Product_ProductId", "public.Products", "ProductId");
            AddForeignKey("public.OrderPurchases", "Order_OrderId", "public.Orders", "OrderId");
            AddForeignKey("public.Orders", "Client_ClientId", "public.Clients", "ClientId");
        }
    }
}
