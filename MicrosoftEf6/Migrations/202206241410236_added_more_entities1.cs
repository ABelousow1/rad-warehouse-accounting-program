﻿namespace MicrosoftEf6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class added_more_entities1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("public.Orders", "OrderingClient_ClientId", "public.Clients");
            DropIndex("public.Products", new[] { "Client_ClientId" });
            DropIndex("public.Orders", new[] { "OrderingClient_ClientId" });
            AddColumn("public.Orders", "Client_ClientId", c => c.Int());
            CreateIndex("public.Orders", "Client_ClientId");
            DropColumn("public.Products", "Client_ClientId");
            DropColumn("public.Orders", "OrderingClient_ClientId");
        }
        
        public override void Down()
        {
            AddColumn("public.Orders", "OrderingClient_ClientId", c => c.Int());
            AddColumn("public.Products", "Client_ClientId", c => c.Int());
            DropIndex("public.Orders", new[] { "Client_ClientId" });
            DropColumn("public.Orders", "Client_ClientId");
            CreateIndex("public.Orders", "OrderingClient_ClientId");
            CreateIndex("public.Products", "Client_ClientId");
            AddForeignKey("public.Orders", "OrderingClient_ClientId", "public.Clients", "ClientId");
        }
    }
}
