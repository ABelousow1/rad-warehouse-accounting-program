﻿namespace MicrosoftEf6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migr21 : DbMigration
    {
        public override void Up()
        {
            AddColumn("public.Orders", "Status", c => c.String());
            DropColumn("public.OrderPurchases", "totalPrice");
        }
        
        public override void Down()
        {
            AddColumn("public.OrderPurchases", "totalPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("public.Orders", "Status");
        }
    }
}
