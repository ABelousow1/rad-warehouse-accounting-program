﻿namespace MicrosoftEf6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migr11 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "public.WareHouses", name: "ProductId", newName: "ProductOnWareHouse_ProductId");
            RenameIndex(table: "public.WareHouses", name: "IX_ProductId", newName: "IX_ProductOnWareHouse_ProductId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "public.WareHouses", name: "IX_ProductOnWareHouse_ProductId", newName: "IX_ProductId");
            RenameColumn(table: "public.WareHouses", name: "ProductOnWareHouse_ProductId", newName: "ProductId");
        }
    }
}
