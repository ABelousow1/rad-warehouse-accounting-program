﻿namespace MicrosoftEf6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migr2 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("public.WareHouses");
            AddPrimaryKey("public.WareHouses", new[] { "ChangeTimeStamp", "ProductOnWareHouse_ProductId" });
        }
        
        public override void Down()
        {
            DropPrimaryKey("public.WareHouses");
            AddPrimaryKey("public.WareHouses", "ChangeTimeStamp");
        }
    }
}
