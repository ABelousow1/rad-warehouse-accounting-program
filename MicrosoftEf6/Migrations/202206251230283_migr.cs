﻿namespace MicrosoftEf6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migr : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("public.InvoicePurchases", "PurchaseInvoice_InvoiceId", "public.PurchaseInvoices");
            DropIndex("public.InvoicePurchases", new[] { "PurchaseInvoice_InvoiceId" });
            RenameColumn(table: "public.InvoicePurchases", name: "PurchaseInvoice_InvoiceId", newName: "PurchaseInvoiceId");
            DropPrimaryKey("public.OrderPurchases");
            DropPrimaryKey("public.PurchaseInvoices");
            AddColumn("public.OrderPurchases", "OrderPurchaseId", c => c.Int(nullable: false, identity: true));
            AddColumn("public.PurchaseInvoices", "PurchaseInvoiceId", c => c.Int(nullable: false, identity: true));
            AlterColumn("public.InvoicePurchases", "PurchaseInvoiceId", c => c.Int(nullable: false));
            AddPrimaryKey("public.OrderPurchases", "OrderPurchaseId");
            AddPrimaryKey("public.PurchaseInvoices", "PurchaseInvoiceId");
            CreateIndex("public.InvoicePurchases", "PurchaseInvoiceId");
            AddForeignKey("public.InvoicePurchases", "PurchaseInvoiceId", "public.PurchaseInvoices", "PurchaseInvoiceId", cascadeDelete: true);
            DropColumn("public.OrderPurchases", "PurchaseId");
            DropColumn("public.PurchaseInvoices", "InvoiceId");
        }
        
        public override void Down()
        {
            AddColumn("public.PurchaseInvoices", "InvoiceId", c => c.Int(nullable: false, identity: true));
            AddColumn("public.OrderPurchases", "PurchaseId", c => c.Int(nullable: false, identity: true));
            DropForeignKey("public.InvoicePurchases", "PurchaseInvoiceId", "public.PurchaseInvoices");
            DropIndex("public.InvoicePurchases", new[] { "PurchaseInvoiceId" });
            DropPrimaryKey("public.PurchaseInvoices");
            DropPrimaryKey("public.OrderPurchases");
            AlterColumn("public.InvoicePurchases", "PurchaseInvoiceId", c => c.Int());
            DropColumn("public.PurchaseInvoices", "PurchaseInvoiceId");
            DropColumn("public.OrderPurchases", "OrderPurchaseId");
            AddPrimaryKey("public.PurchaseInvoices", "InvoiceId");
            AddPrimaryKey("public.OrderPurchases", "PurchaseId");
            RenameColumn(table: "public.InvoicePurchases", name: "PurchaseInvoiceId", newName: "PurchaseInvoice_InvoiceId");
            CreateIndex("public.InvoicePurchases", "PurchaseInvoice_InvoiceId");
            AddForeignKey("public.InvoicePurchases", "PurchaseInvoice_InvoiceId", "public.PurchaseInvoices", "InvoiceId");
        }
    }
}
