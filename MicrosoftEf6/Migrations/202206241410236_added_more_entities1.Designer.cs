﻿// <auto-generated />
namespace MicrosoftEf6.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class added_more_entities1 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(added_more_entities1));
        
        string IMigrationMetadata.Id
        {
            get { return "202206241410236_added_more_entities1"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
