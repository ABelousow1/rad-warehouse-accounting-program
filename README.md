# Rad Warehouse accounting program

Winforms program to manage clients, orders, products and categories of products on a warehouse
Written on C# Winforms with PostgreSQL + EntityFramework.

![Mainwindow](Screenshot.png "Mainwindow")