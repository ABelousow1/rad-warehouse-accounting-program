﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormsApp1.Entities
{
    public class Vendor
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
     
    }
    public class Purchase
    {
        [Key]
        public int PurchaseInvoiceId { get; set; }
        public Product Product { get; set; }
        public int Amount { get; set; }
        public decimal Price { get; set; }
        public decimal? TotalPrice { get; set; }
    }
    public class PurchaseInvoice
    {
        public int Id { get; set; }
        public DateTime DateTime { get; set; }
        public Vendor Vendor { get; set; }
        public ICollection<Purchase> Purchases { get; set; }

    }

    public class WareHouseState
    {
        public DateTime DateTime { get; set; }
        public Product Product { get; set; }
        public int Amount { get; set; }
    }
    public class Client
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class ClientPurchase
    {
        public Order Order { get; set; }
        public Product Product { get; set; }
        public int amount { get; set; }
        public decimal Price { get; set; }
        public decimal? TotalPrice { get; set; }
    }
    public class Order
    {
        [Key]
        public int Id { get; set; }
        public ICollection<ClientPurchase> purchases;
        DateTime date;
        public decimal TotalPrice { get; set; }
    }
}
